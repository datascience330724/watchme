from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime

class elements:
    def __init__(self, driver):
        self.driver = driver

    def accept_cookies(self):
        return self.driver.find_element(By.XPATH, "//*[@data-label='accept-button']")

    def cookies_popup(self):
        return self.driver.find_element(By.XPATH, "//*[@data-name='cookie-consent-layer']")
    
    def buy_a_watch(self):
        return self.driver.find_element(By.XPATH, "//*[@data-flyout-target='#js-header-buy-flyout']")
    
    def mens_watch(self):
        return self.driver.find_element(By.PARTIAL_LINK_TEXT, "Men's Watches")
    
    def sort(self):
        return self.driver.find_element(By.CSS_SELECTOR, ".form-control.result-sorting")
    
    def register_popup(self):
        return self.driver.find_element(By.XPATH, "//*[@data-name='']")
    
    def close_registretion(self):
        return self.driver.find_element(By.CSS_SELECTOR, ".m-r-sm-5.js-close-modal")
    
    def extend_page(self):
        return self.driver.find_element(By.PARTIAL_LINK_TEXT, "120")
    
    def next_page(self):
        return self.driver.find_element(By.CLASS_NAME, "paging-next")
    
    def report_btn(self):
        return self.driver.find_element(By.CSS_SELECTOR, ".js-report-watch.link")
    
    # elements of data
    def data_name(self):
        text = self.driver.find_element(By.CSS_SELECTOR, ".h3.m-y-0").text
        return text.split('\n')[0].strip()

    def data_brand(self):
        try:
            if self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[3]/td[1]").text == "Brand":
                    return self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[3]/td[2]").text
            else:
                return "NaN"
        except:
            return "NaN"
    
    def data_model(self):
        try:
            if self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[4]/td[1]").text == "Model":
                return self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[4]/td[2]").text
            else:
                return "NaN"
        except:
            return "NaN"
        
    def data_case_material(self):
        try:
            if self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[7]/td[1]").text == "Case material":
                return self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[7]/td[2]").text
            else:
                return "NaN"
        except:
            return "NaN"

    def data_bracelet_material(self):
        try:
            if self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[4]/td[1]").text == "Bracelet material":
                return self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[4]/td[2]").text
            else:
                return "NaN"
        except:
            return "NaN"   
    def data_bracelet_color(self):
        try:
            if self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[4]/tr[3]/td[1]").text == "Bracelet color":
                return self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[4]/tr[3]/td[2]").text
            else:
                return "NaN"
        except:
            return "NaN"
    
    def data_year_of_production(self):
        try:
            if self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[9]/td[1]").text == "Year of production":
                return self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[9]/td[2]").text
            else:
                return "Nan"
        except:
            return "NaN"
    
    def data_condition(self):
        try:
            if self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[10]/td[1]").text == "Condition":
                return self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[1]/tr[10]/td[2]").text
            else:
                return "Nan"
        except:
            return "NaN"
        
    def data_case_diameter(self):
        try:
            if self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[3]/tr[2]/td[1]/span").text == "Case diameter":
                return self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[3]/tr[2]/td[2]/span").text
            else:
                return "NaN"
        except:
            return "NaN"
        
    def data_dial_color(self):
        try:
            if self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[3]/tr[3]/td[1]").text == "Dial":
                return self.driver.find_element(By.XPATH, "//*[@class='js-tab tab active']//div[@class='col-xs-24 col-md-12'][1]//tbody[3]/tr[3]/td[2]").text
            else:
                return "NaN"
        except:
            return "NaN"
        
    def data_acquired_date(self):
        return datetime.now().strftime("%Y-%m-%d")
    
    def data_price(self):
        try:
            return float(self.driver.find_element(By.CLASS_NAME, "js-price-shipping-country").text[1:].replace(',',''))
        except:
            try:
                return float(self.driver.find_element(By.XPATH, "//*[@class='price-md']").text[1:].replace(',',''))
            except:
                return 0
